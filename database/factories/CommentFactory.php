<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Comment;
use App\Dress;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'text'=>$faker->text,
        'dress_id'=>factory(Dress::class),
        'client_id'=>factory(Client::class),
    ];
});
