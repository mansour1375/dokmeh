<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Dress;
use App\Store;
use Faker\Generator as Faker;

$factory->define(Dress::class, function (Faker $faker) {
    return [
        'store_id'=> factory(Store::class),
        'size_id'=>1,
        'category_id'=>1,
        'pic_id' =>1,
        'status'=>'4s',
        'name'=>'T-shirt',
        'color'=>'#ffffff',
        'fee'=>200000,
        'description'=>$faker->text,
        'off' => 20
    ];
});
