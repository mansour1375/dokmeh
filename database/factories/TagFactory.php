<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Dress;
use App\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    return [
        'dress_id'=> 1,
        'name'=>'mansour',
    ];
});
