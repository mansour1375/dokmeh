<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dresses', function (Blueprint $table) {
            $table->id();
            $table->integer('store_id')->nullable();
            $table->integer('size_id');//size of the dress
            $table->integer('category_id');//category of dress
            $table->string('pic_id')->nullable(); // pic of the dress
            $table->string('status')->default('sold');  // 1 => in sell / 2 => not for sell yet / 3 => sold
            $table->string('name');
            $table->string('color');
            $table->bigInteger('fee'); // price
            $table->string('description');
            $table->integer('off')->default(0);// percentage %
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dresses');
    }
}
