<?php

namespace Tests\Feature;

use App\Category;
use App\Dress;
use App\Size;
use App\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTest extends TestCase
{
   use RefreshDatabase;

    /*public function test_search_by_tagw()
    {

    }*/
    public function test_search_by_tag()
    {
        $tag = factory(Tag::class)->create();
        $tag1 = factory(Tag::class)->create(['name'=>'mars']);
        $tag2 = factory(Tag::class)->create(['name'=>'mark']);
        $tag3 = factory(Tag::class)->create(['name'=>'mansour2']);

        $tag4 = factory(Tag::class)->create();
        $tag5 = factory(Tag::class)->create(['name'=>'mars']);
        $tag6 = factory(Tag::class)->create(['name'=>'mark']);
        $tag7 = factory(Tag::class)->create(['name'=>'mansour2']);

        $tag8 = factory(Tag::class)->create(['name'=>'mars']);

        $dress1= factory(Dress::class)->create();

        $dress1->tags()->save($tag4);
        $dress1->tags()->save($tag5);
        $dress1->tags()->save($tag6);
        $dress1->tags()->save($tag7);

        $dress = factory(Dress::class)->create(['name'=>'shalvar']);

        $dress->tags()->save($tag);
        $dress->tags()->save($tag1);
        $dress->tags()->save($tag2);
        $dress->tags()->save($tag3);

        $dress2 = factory(Dress::class)->create(['name'=>'joorab','status'=>'n4s']);
        $dress2->tags()->save($tag8);

        $response = $this->get('api/Search/tag/ma');

        $response->assertStatus(200);


    }


    public function test_search_by_size(){

        $size =factory(Size::class)->create(['name'=>'32']);
        $dress = factory(Dress::class)->create(['name'=>'shalvar','size_id'=>$size->id]);
        $dress2 = factory(Dress::class)->create(['name'=>'shalvar1','size_id'=>$size->id]);

        $response = $this->get('api/Search/size/'.$size->id);
        $this->assertCount(2,Dress::all());
        $response->assertJson([
           ['id'=>1],
           ['id'=>2],
        ]);

    }


    public function test_search_by_category(){


        $category = factory(Category::class)->create();
        $dress = factory(Dress::class)->create(['name'=>'shalvar']);
        $dress2 = factory(Dress::class)->create(['name'=>'shalvar1']);
        $dress2->category_id = $category->id;
        $dress2->save();
        $dress->category_id = $category->id;
        $dress->save();


        $response = $this->get('api/Search/category/'.$category->id);
        $response->assertJson([
            ['id'=>1],
            ['id'=>2]
        ]);

    }


    public function test_search_by_fee()
    {
        $dress =factory(Dress::class)->create(['fee'=>23000]);
        $dress1 =factory(Dress::class)->create(['fee'=>35000]);
        $dress2 =factory(Dress::class)->create(['fee'=>55000]);

        $response = $this->get('api/Search/fee/'. 55000 .'/'. 25000 );
        $response->assertJson([
            ['id'=>2],
            ['id'=>3],
        ]);
    }


}
