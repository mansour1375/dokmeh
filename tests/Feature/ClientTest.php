<?php

namespace Tests\Feature;

use App\Client;
use App\Dress;
use App\Store;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ClientTest extends TestCase
{

    use RefreshDatabase;
    protected $user;
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['role_id'=>4]);
    }

    //store
    public function test_a_Client_can_be_added_by_anyone()
    {
        $this->withExceptionHandling();
        $response = $this->post('api/Client',$this->data());
        $this->assertEquals(1,Client::all()->count());
        $this->user->refresh();
        $this->assertEquals(3,$this->user->role_id);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    //update
    public function test_a_Client_can_be_edit_by_owner()
    {
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->patch('api/Client/'.$client->id,$this->data());
        $client->refresh();
        $this->assertEquals('mansour',$client->name);
        $this->assertEquals('some url',$client->pic);
        $response->assertStatus(Response::HTTP_OK);
    }

    //show
    public function test_a_Client_can_be_retrieved_by_admins_and_owner()
    {
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->get('api/Client/'.$client->id.'?api_token='.$this->user->api_token);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertJson([

                'user_id'=>$this->user->id,
                'name'=>$client->name,

        ]);

    }

    public function test_a_Client_can_be_deleted_by_admin()
    {
        $this->user->role_id = 3;
        $this->user->save();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);

        $response = $this->delete('api/Client/Admin/'.$client->id.'?api_token='.$this->user->api_token);

        $this->assertEquals(1,Client::onlyTrashed()->count());
        $this->assertEquals(0,Client::withoutTrashed()->count());
        $this->user->refresh();
        $this->assertEquals(4,$this->user->role_id);

        ;
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }


    public function test_a_Client_can_be_destroyed_by_admin()
    {

        $this->user->role_id = 3;
        $this->user->save();

        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->delete('api/Client/Admin/destroy/'.$client->id.'?api_token='.$this->user->api_token);
        $this->user->refresh();
        $this->assertEquals(4,$this->user->role_id);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertEquals(0,Client::withTrashed()->count());

    }


    public function test_a_Client_can_be_restored_by_admin()
    {

        $this->user->role_id =1 ;
        $this->user->save();

        $user = factory(User::class)->create(['role_id'=>4]);

        $client = factory(Client::class)->create(['user_id'=>$user->id]);
        $client->delete();
        $response = $this->get('api/Client/Admin/restore/'.$client->id.'?api_token='.$this->user->api_token);
        $user->refresh();
        $this->assertEquals(3,$user->role_id);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals(0,Client::onlyTrashed()->count());

    }




    //Stores
    //follow
    public function test_client_follows_Store()
    {
        $store = factory(Store::class)->create();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->get('api/Client/follow/'.$store->id.'/'.$client->id.'?api_token='.$this->user->api_token);
        $client->refresh();
        $this->assertEquals(1,$client->Stores()->count());
    }

    //unfollow
    public function test_client_unfollows_the_Store()
    {
        $store = factory(Store::class)->create();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $client->Stores()->save($store);

        $response = $this->delete('api/Client/follow/'.$store->id.'/'.$client->id.'?api_token='.$this->user->api_token);
        $this->assertEquals(0,$client->Stores()->count());
        $response->assertJson([
            'id'=>$store->id
        ]);
    }

    //follow list
    public function test_show_client_favorite_stores()
    {
        $store = factory(Store::class)->create();
        $store1 = factory(Store::class)->create();
        $store2 = factory(Store::class)->create();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $client->Stores()->save($store);
        $client->Stores()->save($store1);
        $client->Stores()->save($store2);

        $response = $this->get('api/Client/Stores/'.$client->id.'?api_token='.$this->user->api_token);

        $response->assertJson([
            ['id'=>$store->id],['id'=>$store1->id],['id'=>$store2->id]
        ]);
    }

    //Dresses
    //liked list
    public function test_show_client_favorite_dresses()
    {
        $dress = factory(Dress::class)->create();
        $dress1 = factory(Dress::class)->create();
        $dress2 = factory(Dress::class)->create();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $client->dresses()->save($dress);
        $client->dresses()->save($dress1);
        $client->dresses()->save($dress2);
        $this->assertCount(3,$client->dresses()->get());
        $response = $this->get('api/Client/likes/'.$client->id.'?api_token='.$this->user->api_token);

        $response->assertJson([
            ['id'=>$dress->id],['id'=>$dress1->id],['id'=>$dress2->id]
        ]);
    }

//like the dress
    public function test_client_likes_a_dress()
    {
        $dress = factory(Dress::class)->create();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->get('api/Client/like/'.$dress->id.'/'.$client->id.'?api_token='.$this->user->api_token);
        $client->refresh();
        $this->assertEquals(1,$client->dresses()->count());
    }

    //dislike the dress
    public function test_client_dislikes_a_dress()
    {
        $dress = factory(Dress::class)->create();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $client->dresses()->save($dress);
        $response = $this->delete('api/Client/like/'.$dress->id.'/'.$client->id.'?api_token='.$this->user->api_token);
        $client->refresh();
        $this->assertEquals(0,$client->dresses()->count());
    }

    public  function  data(){
        return
        [
            'name'=>'mansour',
            'pic'=>'some url',
            'address'=>'iran ahvaz',
            'valet'=>2000000,
            'phone'=>'09161114651',
            'api_token'=>$this->user->api_token,
        ];
    }
}
