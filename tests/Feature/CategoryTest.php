<?php

namespace Tests\Feature;

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user= factory(User::class)->create(['role_id'=>1]);
    }

    public function test_all_categories_can_be_retrieved_by_anyone()
    {

        $cat1 = factory(Category::class)->create();
        $cat2 = factory(Category::class)->create();

        $response = $this->get('api/category');
        $response->assertStatus(200);
        $response->assertJson([
           ['id'=>$cat1->id],
           ['id'=>$cat2->id]
        ]);

    }

    public function test_a_category_can_be_added()
    {
        $response = $this->post('api/category',$this->data());
        $this->assertCount(1,Category::all());

    }

    public function test_a_category_can_be_deleted()
    {
        $category = factory(Category::class)->create();
        $response = $this->delete('api/category/'.$category->id.'?api_token='.$this->user->api_token);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertCount(0,Category::all());

    }

    public function test_a_category_can_be_edited()
    {
        $category = factory(Category::class)->create();
        $response = $this->patch('api/category/'.$category->id,$this->data());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
           'name'=>'men-jeans'
        ]);
    }


    public function data(){
        return [
            'name'=>'men-jeans',
            'api_token'=>$this->user->api_token,
        ];
    }

}
