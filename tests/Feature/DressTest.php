<?php

namespace Tests\Feature;

use App\Dress;
use App\Store;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class DressTest extends TestCase
{
    use RefreshDatabase;


    protected $user;
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create(['role_id'=>2]);
    }

    public function test_a_stores_dress_can_retrieved_by_anyone()
    {

        //index a dress have seen by anyone
        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $otherdress = factory(Dress::class)->create(['store_id'=>$store->id]);

        $response =$this->get('api/Dresses/Store/'.$store->id );
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertJson([
               [
               'id'=>1
               ],
               [
                'id'=>2
               ]
        ]);
        $this->assertCount(2 , Dress::all());

    }
    public function test_a_dress_can_be_added_by_store()
    {

        $response = $this->post('api/Dresses',$this->data());
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertCount(1,Dress::all());


    }

    public function test_a_dress_can_be_edited_by_Authenticated_seller ()
    {
        //show
        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $response = $this->patch('api/Dresses/'.$dress->id , array_merge($this->data(),['name'=>'name changed']));
        $response->assertStatus(Response::HTTP_OK);
        $dress = Dress::first();
        $this->assertEquals($dress->name,'name changed');

    }

    public function test_a_dress_can_be_retrieved_by_anyone()
    {
        //show
        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $response = $this->get('api/Dresses/'.$dress->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertJson([
           [
               'id'=> 1,
               'name'=>$dress->name,
               'status'=>$dress->status,
           ]
        ]);


    }
    public function test_a_dress_can_be_retrieved_by_admin()
    {
        //show
        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $response = $this->get('api/Dresses/Store/'.$dress->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertJson([
           [
               'id'=> 1,
               'name'=>$dress->name,
               'status'=>$dress->status,
           ]
        ]);


    }
    public function test_a_dress_can_be_deleted_softly_by_store()
    {

        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $response = $this->delete('api/Dresses/'.$dress->id.'?api_token='.$this->user->api_token);
        $this->assertEquals(0,Dress::withoutTrashed()->get()->count());

    }
    public function test_a_dress_can_be_destroy_by_store()
    {
        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $response = $this->delete('api/Dresses/destroy/'.$dress->id.'?api_token='.$this->user->api_token);
        $this->assertEquals(0,Dress::onlyTrashed()->get()->count());

    }

    public function test_a_dress_can_be_restore_by_store()
    {
        $store = factory(Store::class)->create(['user_id'=>$this->user->id]);
        $dress = factory(Dress::class)->create(['store_id'=>$store->id]);
        $dress->delete();
        $response = $this->get('api/Dresses/Store/restore/'.$dress->id.'?api_token='.$this->user->api_token);
        $this->assertEquals(1,Dress::withoutTrashed()->get()->count());

    }




    public function fields_are_required()
    {
        collect(['status', 'name', 'color', 'fee', 'description', 'off' ,])
            ->each(function ($field){
                $response = $this->post('/api/Dresses', array_merge($this->data() , [$field=>'']));



                $response->assertSessionHasErrors($field);
                $this->assertCount(0,Dress::all());
            });
    }


    public  function data(){
        return [
            'store_id'=> factory(Store::class)->create(['user_id'=>$this->user->id]),
            'size_id'=>1,
            'category_id'=>1,
            'pic_id' =>1,
            'status'=>'forsale',
            'name'=>'T-shirt',
            'color'=>'#ffffff',
            'fee'=>200000,
            'description'=>'text',
            'off' => 20,
            'api_token'=>$this->user->api_token
        ];
    }
}
