<?php

namespace Tests\Feature;

use App\Client;
use App\Comment;
use App\Dress;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;
    protected $user;
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function test_a_comment_can_be_added()
    {
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->post('api/comment/',$this->data());
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertCount(1,Comment::all());

    }

    public function test_a_comment_can_be_replied()
    {
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $parent_comment = factory(Comment::class)->create();

        $response = $this->post('api/comment/',array_merge($this->data()
                                       ,['parent_id'=>$parent_comment->id]));

        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertCount(2,Comment::all());
        $this->assertCount(1,Comment::query()->where('parent_id',$parent_comment->id)->get());

    }

    public function test_a_comment_can_be_added_denied()
    {
        $this->user->role_id = 4;
        $this->user->save();
        $client = factory(Client::class)->create(['user_id'=>$this->user->id]);
        $response = $this->post('api/comment/',$this->data());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertCount(0,Comment::all());

    }

    public function test_a_comment_can_be_retrieved()
    {
        $dress= factory(Dress::class)->create();
        $comment = factory(Comment::class)->create(['client_id'=>1,'dress_id'=>$dress->id]);
        $comment1 = factory(Comment::class)->create(['client_id'=>2,'dress_id'=>$dress->id]);

        $response =$this->get('api/comment/'.$dress->id.'?api_token='.$this->user->api_token);
        $response->assertJson([
            ['id'=>$comment->id,'text'=>$comment->text],
            ['id'=>$comment1->id,'text'=>$comment1->text]
        ]);


    }

    public function test_a_comment_can_be_deleted_by_admin()
    {
        $this->user->role_id = 1;
        $this->user->save();
        $dress= factory(Dress::class)->create();
        $comment = factory(Comment::class)->create(['client_id'=>1,'dress_id'=>$dress->id]);
        $response =$this->delete('api/comment/'.$comment->id.'?api_token='.$this->user->api_token);
        $this->assertCount(0,Comment::all());

    }

    public function data(){
        return[
            'dress_id'=> 1,
            'text'=>'this dress is beautiful!',
            'api_token'=>$this->user->api_token
        ];

    }

}
