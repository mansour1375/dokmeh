<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;

    protected $guarded=[];

   public function user()
   {
       return $this->belongsTo(User::class);
   }

   public function dresses()
   {

       return $this->hasMany(Dress::class);
   }

   public function Clients()
   {

        return $this->belongsToMany('App\Client','ClientStore',
            'store_id','client_id');
   }
}
