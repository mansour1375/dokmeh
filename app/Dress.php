<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Dress extends Model
{
    protected $guarded=[];
    use SoftDeletes;



    public function tags()
    {
      return $this->hasMany(Tag::class,'dress_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function clients()
    {
        return $this->belongsToMany('App\Client','ClientDress','dress_id','client_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'dress_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }




}
