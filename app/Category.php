<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded=[];

    public function dresses()
    {
        return $this->hasMany(Dress::class , 'category_id');
    }
}
