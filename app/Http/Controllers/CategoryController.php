<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    public function index(){
        //policy
        return response(Category::all(),Response::HTTP_OK);
    }

    public function store(){

        $category = new Category($this->validateData());
        $category->save();
    }

    public function update(Category $category) {
        $category->update($this->validateData());
        return \response($category,Response::HTTP_OK);
    }

    public function destroy(Category $category)
    {

        $category->delete();

        return \response([],Response::HTTP_NO_CONTENT);

    }
    public function validateData(){
        return \request()->validate([
            'name'=>'required',

        ]);
    }
}
