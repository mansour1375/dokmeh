<?php

namespace App\Http\Controllers;

use App\Dress;
use App\Store;
use http\Client\Curl\User;
use App\User as userClass;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class StoreController extends Controller
{
    //api

    // return the list of stores
    //for admin
    public function index()
    {
        $this->authorize('viewany',Store::class);
        //all users
        //need resource
        return Store::withoutTrashed()->get();
    }

    public function trashed()
    {
        $this->authorize('viewAdmin',Store::class);
        //admin
        //need resource
        return Store::onlyTrashed()->get();
    }

    public function  all()
    {
        $this->authorize('viewAdmin',Store::class);
        //admin
        //need resource
        return Store::withTrashed()->get();
    }



    //return the store's details
    //save the store in database
    public function store()
    {


        $this->authorize('create',Store::class);

        $store = \request()->user()->Store()->create($this->validateData());
        $user = \App\User::find($store->user_id);
        $user->role_id = 2;
        $user->save();

        return \response($store ,Response::HTTP_CREATED);
    }

    //returns the store's details & the store's dresses

    public function show(Store $store)
    {
        $this->authorize('view',Store::class);

        return response(Store::withoutTrashed()->find($store),Response::HTTP_FOUND) ;
    }

    //returns the store's details
    //update the store info
    public function update(Store $store)
    {

        $this->authorize('update', $store);


        $store->update($this->validateData());
        return response($store,Response::HTTP_OK);
    }

    //deactivate the store
    public function delete(Store $store)
    {

        $this->authorize('delete',$store);

        $user = \App\User::find($store->user_id);
        $user->role_id = 4;
        $user->save();

        $store->delete();

        //redirect to home
        return response([],Response::HTTP_NO_CONTENT);

    }

    //delete the store
    public function destroy(int $id)
    {
        $this->authorize('forceDelete',Store::class);

        $store = Store::withTrashed()->findOrFail($id);

        $store->forceDelete();
        return response([],Response::HTTP_NO_CONTENT);
    }


    //restore the store
    public function restore(int $id)
    {
         $this->authorize('restore',Store::class);
         $store = Store::onlyTrashed()->find($id);
         $store->restore();


         $user = \App\User::find($store->user_id);
         $user->role_id = 2;
         $user->save();

        return response([],Response::HTTP_OK);
    }



    public function validateData(){
        return \request()->validate([
            'name'=>'required',
            'logo'=>'required',
            'description'=>'required ',
            'bio'=>'required',
            'address'=>'required',

        ]);
    }


}
