<?php

namespace App\Http\Controllers;

use App\Dress;
use App\Tag;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    public function index(string $name)
    {

        $tags = Tag::query()->where('name','LIKE','%'.$name.'%')->get();

        return response($tags,Response::HTTP_FOUND);
    }


    public function show(Dress $dress)
    {
        if (!empty($dress))
        {
            return \response($dress->tags(),Response::HTTP_FOUND);
        }

        return \response([],Response::HTTP_NOT_FOUND);
    }


    public function store()
    {
        $tag = new Tag($this->validateData());
        $tag->save();
        return \response($tag,Response::HTTP_CREATED);
    }


    public function destroy(Tag $tag)
    {

        $tag->delete();
        return \response([],Response::HTTP_NO_CONTENT);
    }


    public function validateData(){
        return \request()->validate([
           'name'=>'required',
           'dress_id'=>'required',

        ]);
    }
}
