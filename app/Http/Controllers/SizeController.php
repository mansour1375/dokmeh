<?php

namespace App\Http\Controllers;

use App\Size;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SizeController extends Controller
{
    public function index()
    {
        return response(Size::all(),Response::HTTP_OK);
    }

    public function store()
    {
        $size = new Size($this->validateData());
        $size->save();
        return \response($size,Response::HTTP_CREATED);
    }

    public function destroy(Size $size){
        dd($size);
        $size->delete();
        return \response([],Response::HTTP_NO_CONTENT);

    }



    public function validateData(){
        return \request()->validate([
            'name'=>'required'
        ]);
    }
}
