<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dress;
use App\Size;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use function MongoDB\BSON\toJSON;

class SearchController extends Controller
{
    //initial the home pages
    public function index()
    {
        return response(Dress::withoutTrashed()->where('status','4s')->orderBy('created_at'),Response::HTTP_FOUND);
    }

    public function tag(string $tag)
    {
        //$tags = DB::table('dresses')->Join('tags','dresses.id','=','tags.dress_id')/*->where('tags.name','LIKE','%'.$tag.'%')*/->select('dresses.name')->get();
        $dresses = DB::table('tags')->Join('dresses','dresses.id','=','tags.dress_id')
            /*->select('dresses.*')*/
            ->where('tags.name','LIKE','%'.$tag.'%')
            ->where('dresses.deleted_at','=',null)
            ->where('dresses.status','=','4s')
            ->get();
        $dresses = $dresses->unique('id');

        return \response($dresses,Response::HTTP_OK);
    }

    public function category(Category $category)
    {
        $dresses =Dress::withoutTrashed()->where('category_id',$category->id)->get();
        return \response($dresses,Response::HTTP_OK);
    }

    public function Size(Size $size)
    {

        $dresses = Dress::withoutTrashed()->where('size_id',$size->id)->where('status','=','4s')->get();

        return \response($dresses,Response::HTTP_OK);
    }

    public function fee(int $max ,int $min)
    {
        if ($max>$min)
            $dresses = Dress::withoutTrashed()->where('fee','<=',$max)->where('fee','>=',$min)->get();


        return \response($dresses,Response::HTTP_OK);
    }
}
